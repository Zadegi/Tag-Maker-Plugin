<?php

add_action( 'init', 'myStartSession', 1 );
add_action( 'wp_logout', 'myEndSession' );
add_action( 'wp_login', 'myEndSession' );

function myStartSession() {
	if ( ! session_id() ) {
		session_start();
	}
}

function myEndSession() {
	session_destroy();
}

$set_view_tag_meta_box = 0;
$set_view_fa_text_box = 0;
$set_view_en_text_box = 0;
//**
global $wpdb, $table_prefix;
$query = "select * from {$table_prefix}tagm_tag_settings";
$tag_settings = $wpdb->get_results($query);

if($tag_settings && count($tag_settings) == 3) :
	$set_view_tag_meta_box = $tag_settings[0]->tag_setting_status;
	$set_view_fa_text_box = $tag_settings[1]->tag_setting_status;
	$set_view_en_text_box = $tag_settings[2]->tag_setting_status;
endif;

//--------------------------------------------------------------
function tag_maker_box_html( $post ) {
	global $wpdb, $table_prefix;
	$query      = "select * from {$table_prefix}tagm_tag_themes";
	$tag_themes = $wpdb->get_results( $query );

	global $set_view_fa_text_box, $set_view_en_text_box;
	tags_load_view('post.index', compact('post',
		'tag_themes',
		'set_view_fa_text_box',
		'set_view_en_text_box'));

}

//--------------------------------------------------------
function tag_maker_add_custom_box() {
	$screens = [ 'post' ];  //'post'نوشته ها   'page' برگه ها
	foreach ( $screens as $screen ) {
		add_meta_box(
			'tag_maker_box_id',        // Unique ID
			'تگ ساز',                 // Box title
			'tag_maker_box_html',  // Content callback, must be of type callable
			$screen                       // Post type
		);
	}
}

if($set_view_tag_meta_box == 1)
{
	add_action('add_meta_boxes', 'tag_maker_add_custom_box');
}

add_action( 'admin_footer', 'my_action_javascript' ); // Write our JS below here

function my_action_javascript() { ?>
    <script type="text/javascript">
        function set_textarea() {
            var $this = document.getElementById("tag_themes_list").value;
            var data = {
                'action': 'my_action',
                'item': $this
            };
            jQuery.post(ajaxurl, data, function (response) {
                $("textarea#tagList").val(response);
            });
        };
        jQuery(document).ready(function ($) {
            set_textarea();
        });
        jQuery(document).ready(function ($) {
            $('#tag_themes_list').on('change', function () {
                set_textarea();
            });
        });
    </script> <?php
}

add_action( 'wp_ajax_my_action', 'my_action' );

function my_action() {
	global $wpdb, $table_prefix;

	$id = intval( $_POST['item'] );

	$query     = "select tag_theme_item_tit from {$table_prefix}tagm_tag_theme_items where tag_theme_IDRef = {$id}";
	$tag_items = $wpdb->get_results( $query );

	$result = "";
	if ( $tag_items && count( $tag_items ) > 0 ) :
		foreach ( $tag_items as $tag_item ) {
			$result = $result . (string) $tag_item->tag_theme_item_tit;
		}
	endif;
	echo $result;

	wp_die(); // this is required to terminate immediately and return a proper response
}


add_action( 'admin_footer', 'tag_create_javascript' ); // Write our JS below here

function tag_create_javascript() { ?>
    <script type="text/javascript">
        function clear_tag(but) {
            var current_id = '#' + but.id;
            var parent_id = '#' + $(current_id).parent().attr('id');

            var data = {
                'action': 'clear_tag',
                'tag_value': document.getElementById(but.id).value
            };
            jQuery.post(ajaxurl, data, function (response) {
                document.getElementById("tag_list_final_hidden").value = response;
                $(parent_id).remove();
                $(current_id).remove();
            });
        };

        function ajax_tag_create() {
            var $fa_name = "";
            var $en_name = "";
            var $fa_music = "";
            var $en_music = "";
            if( $('#fa_name').length > 0) {
                $fa_name = document.getElementById("fa_name").value;
            }
            if($('#en_name').length > 0) {
                $en_name = document.getElementById("en_name").value;
            }
            if($('#fa_music').length > 0) {
                $fa_music = document.getElementById("fa_music").value;
            }
            if($('#en_music').length > 0) {
                $en_music = document.getElementById("en_music").value;
            }

            var $tagList = document.getElementById("tagList").value;
            var $tag_list_final_hidden = document.getElementById("tag_list_final_hidden").value;
            var data = {
                'action': 'tag_create',
                'fa_name': $fa_name,
                'en_name': $en_name,
                'fa_music': $fa_music,
                'en_music': $en_music,
                'tagList': $tagList,
                'tag_list_final_hidden': $tag_list_final_hidden
            };
            jQuery.post(ajaxurl, data, function (response) {
                //document.getElementById("tagList").value = response;
                document.getElementById("tag_list_final_hidden").value = response;
                var arr_tag = response.split(",");

                document.getElementById("tag_ul_list").innerHTML = "";

                for (var i = 0; i <= arr_tag.length - 1; i++) {
                    var rand_val = (Math.floor(Math.random() * 900000000) + Math.floor(Math.random() * Math.floor(Math.random() * 8700))).toString();
                    var li_id = "custom_tag_li" + rand_val;
                    var btn_id = "custom_tag_btn" + rand_val;
                    $("#tag_list_created ul").append('<li id="' + li_id + '"><button value="' + arr_tag[i].toString() + '"' +
                        ' type="button" id="' + btn_id + '" onclick="clear_tag(this);" class="ntdelbutton">' +
                        '<span class="remove-tag-icon" aria-hidden="true"></span>' +
                        '<span class="screen-reader-text">' + arr_tag[i].toString() + '</span>' +
                        '</button>&nbsp;' + arr_tag[i].toString() + '</li>');
                }

            });
        };

        jQuery(document).ready(function ($) {
            $('#tag_create_btn').on('click', function (e) {
                e.preventDefault();
                ajax_tag_create();
            });
        });
    </script> <?php
}

//***
add_action( 'wp_ajax_tag_create', 'tag_create' );
//***
function tag_create() {
	$fa_name               = $_POST['fa_name'];
	$en_name               = $_POST['en_name'];
	$fa_music              = $_POST['fa_music'];
	$en_music              = $_POST['en_music'];
	$tagList               = $_POST['tagList'];
	$tag_list_final_hidden = $_POST['tag_list_final_hidden'];

	$tagList = str_replace( "{name}", $fa_name, $tagList );
	$tagList = str_replace( "{enname}", $en_name, $tagList );
	$tagList = str_replace( "{music}", $fa_music, $tagList );
	$tagList = str_replace( "{enmusic}", $en_music, $tagList );

	$tagList = str_replace( "\n", ",", $tagList );

	//----------------------------------------
	$arr_text_area = explode( ",", $tagList );
	$arr_sum       = $arr_text_area;

	if ( $tag_list_final_hidden != "" ) {
		$arr_html_input = explode( ",", $tag_list_final_hidden );
		$arr_sum        = array_merge( array_diff( $arr_html_input, $arr_text_area ), $arr_text_area );
	}

	$tagList_final = implode( ",", $arr_sum );

	$_SESSION['tagList']      = $tagList_final;
	$_SESSION['has_tag_save'] = "1";

	echo $tagList_final;

	wp_die(); // this is required to terminate immediately and return a proper response
}

//--------------------------------------------------------------------------------------
add_action( 'wp_ajax_clear_tag', 'clear_tag' );
function clear_tag() {
	$tag_value = $_POST['tag_value'];
	$tagList   = $_SESSION['tagList'];

	$arr = explode( ",", $tagList );

	$key = array_search( $tag_value, $arr );
	if ( $key != null ) {
		unset( $arr[ $key ] );
	}

	$tagList_final = implode( ",", $arr );

	$_SESSION['tagList'] = $tagList_final;

	echo $tagList_final;
	wp_die();
}

//--------------------------------------------------------------------------------------
add_action( 'save_post', 'on_post_publish' );

function on_post_publish( $ID ) {
	// A function to perform actions when a post is published.
	global $post;
	if ( isset( $_SESSION['has_tag_save'] ) && isset( $_SESSION['tagList'] ) ) {
		if ( $_SESSION['has_tag_save'] == "1" ) :
			$val_tag = $_SESSION['tagList'];
			wp_set_post_tags( $post->ID, $val_tag, true );
			$_SESSION['has_tag_save'] = "0";
		endif;
	}
}


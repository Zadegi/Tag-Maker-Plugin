<?php


function tag_maker_menu() {

	global $wpdb, $table_prefix;

	if ( isset( $_GET['tid'] ) && isset( $_GET['mode'] ) ) :
		$where_arr = [
			'tag_theme_ID' => $_GET['tid']
		];
		$wpdb->delete( $table_prefix . 'tagm_tag_themes', $where_arr );
		wp_redirect(home_url( $wp->request ) . '/wp-admin/options-general.php?page=tag_maker_menu');
	endif;

	if ( isset( $_POST['save_settings'] ) ) :
		for ( $index = 1; $index <= 6; $index ++ ) :
			$where_arr = [
				'tag_setting_ID' => $index
			];
			if ( isset( $_POST["checkbox{$index}"] ) ) :
				$data = [
					'tag_setting_status' => true
				];
			else :
				$data = [
					'tag_setting_status' => false
				];
			endif;

			$wpdb->update( $table_prefix . 'tagm_tag_settings', $data, $where_arr );
			wp_redirect(home_url( $wp->request ) . '/wp-admin/options-general.php?page=tag_maker_menu');
		endfor;
	endif;

	if ( isset( $_POST['save_tag_add_edit'] ) ) :
		$mode = 0;  // new;
		if ( isset( $_GET['tid'] ) ) :  // edit
			$query = "select * from {$table_prefix}tagm_tag_themes where tag_theme_ID = {$_GET['tid']}";
			$theme = $wpdb->get_results( $query );
			if ( $theme && count( $theme ) > 0 ) :
				if ( $theme[0]->tag_theme_title == $_POST['themeName'] ) : // edit mode
					$mode = 1; //edit
				endif;
			endif;
		endif;

		$tid     = 0;
		$arr_tag = explode( "\n", $_POST['tagList'] );

		switch ( $mode ) :
			case 0:
				$data = [
					'tag_theme_no'    => count( $arr_tag ),
					'tag_theme_title' => $_POST['themeName']
				];

				$wpdb->insert( $table_prefix . 'tagm_tag_themes',
					$data );
				$tid = $wpdb->insert_id;
				break;
			case 1:
				$where_arr = [
					'tag_theme_IDRef' => $_GET['tid']
				];
				$wpdb->delete( $table_prefix . 'tagm_tag_theme_items', $where_arr );

				$tid = $_GET['tid'];
				break;
		endswitch;

		for ( $index = 0; $index < count( $arr_tag ); $index ++ ) :
			$data = [
				'tag_theme_IDRef'    => $tid,
				'tag_theme_item_tit' => $arr_tag[ $index ]
			];

			$wpdb->insert( $table_prefix . 'tagm_tag_theme_items',
				$data );
		endfor;
	endif;
	//----------------------------------
	add_options_page( 'تگ ساز',
		'تگ ساز',
		'manage_options',
		'tag_maker_menu',
		'tag_maker_page_options' );
}

add_action( 'admin_menu', 'tag_maker_menu' );

function tag_maker_page_options() {
	if ( ! current_user_can( 'manage_options' ) ) {
		wp_die( __( 'برای دسترسی به این صفحه مجوز کافی ندارید.' ) );
	}

	global $wpdb, $table_prefix;
	$query    = "select tag_setting_ID, tag_setting_title, tag_setting_status from {$table_prefix}tagm_tag_settings";
	$settings = $wpdb->get_results( $query );

	$query  = "select * from {$table_prefix}tagm_tag_themes";
	$themes = $wpdb->get_results( $query );

	tags_load_view( 'admin-page.index', compact( 'settings', 'themes' ) );
}
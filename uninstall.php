<?php
// If uninstall is not called from WordPress, exit
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit();
}

//remove tables from database
global $wpdb;
$tbl_tagm_tag_settings = $wpdb->prefix . 'tagm_tag_settings';
$tbl_tagm_tag_themes = $wpdb->prefix . 'tagm_tag_themes';
$tbl_tagm_tag_theme_items = $wpdb->prefix . 'tagm_tag_theme_items';

$wpdb->query("DROP TABLE IF EXISTS {$tbl_tagm_tag_settings}");
$wpdb->query("DROP TABLE IF EXISTS {$tbl_tagm_tag_theme_items}");
$wpdb->query("DROP TABLE IF EXISTS {$tbl_tagm_tag_themes}");

//remove options from database
delete_option('tags_options');
delete_option('tags_db_version');
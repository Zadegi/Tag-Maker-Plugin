<?php
global $tags_db_version;
$tags_db_version = '1.0';

$current_db_version = get_option('tags_db_version');
if(intval($tags_db_version) > intval($current_db_version)){
	tags_install();
	tags_install_data();
}

function tags_install() {
	global $wpdb;
	global $tags_db_version;

	$tbl_tagm_tag_settings = $wpdb->prefix . 'tagm_tag_settings';
	$tbl_tagm_tag_themes = $wpdb->prefix . 'tagm_tag_themes';
	$tbl_tagm_tag_theme_items = $wpdb->prefix . 'tagm_tag_theme_items';
	//---------------------------------------------------------
	$charset_collate = $wpdb->get_charset_collate();
	//---------------------------------------------------------

	$sql_tagm_tag_settings = "CREATE TABLE {$tbl_tagm_tag_settings} (
		  tag_setting_ID int(11) NOT NULL,
          tag_setting_title varchar(100) COLLATE utf8_persian_ci NOT NULL,
          tag_setting_status bit(1) NOT NULL
          ) $charset_collate;";

	$sql_tagm_tag_settings_alter = "ALTER TABLE {$tbl_tagm_tag_settings}
                                    ADD PRIMARY KEY (tag_setting_ID);";

	//---------------------------------------------------------

	$sql_tagm_tag_themes = "CREATE TABLE {$tbl_tagm_tag_themes} (
            tag_theme_ID int(11) NOT NULL,
            tag_theme_title varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
            tag_theme_no int(11) NOT NULL
          ) $charset_collate;";

	$sql_tagm_tag_themes_alter1 = "ALTER TABLE {$tbl_tagm_tag_themes}
                                   ADD PRIMARY KEY (tag_theme_ID);";
	$sql_tagm_tag_themes_alter2 = "ALTER TABLE {$tbl_tagm_tag_themes}
                                   MODIFY tag_theme_ID int(11) NOT NULL AUTO_INCREMENT,
                                   AUTO_INCREMENT=1;";

	//---------------------------------------------------------

	$sql_tagm_tag_theme_items = "CREATE TABLE {$tbl_tagm_tag_theme_items} (
           tag_theme_item_ID int(11) NOT NULL,
           tag_theme_IDRef int(11) DEFAULT NULL,
           tag_theme_item_tit varchar(100) COLLATE utf8_persian_ci DEFAULT NULL
          ) $charset_collate;";

	$sql_tagm_tag_theme_items_alter1 = "ALTER TABLE {$tbl_tagm_tag_theme_items}
                                        ADD PRIMARY KEY (tag_theme_item_ID),
                                        ADD KEY tag_theme_IDRef (tag_theme_IDRef);";

	$sql_tagm_tag_theme_items_alter2 = "ALTER TABLE {$tbl_tagm_tag_theme_items}
                                        MODIFY tag_theme_item_ID int(11) NOT NULL AUTO_INCREMENT,
                                        AUTO_INCREMENT=1;";

	$sql_tagm_tag_theme_items_alter3 = "ALTER TABLE {$tbl_tagm_tag_theme_items}
                                        ADD CONSTRAINT {$wpdb->prefix}tagm_tag_theme_items_ibfk_1
                                        FOREIGN KEY (tag_theme_IDRef) REFERENCES {$wpdb->prefix}tagm_tag_themes (tag_theme_ID) 
                                        ON DELETE CASCADE;";

	//---------------------------------------------------------
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql_tagm_tag_settings );
	dbDelta( $sql_tagm_tag_themes );
	dbDelta( $sql_tagm_tag_theme_items );

	$wpdb->query($sql_tagm_tag_settings_alter);

	$wpdb->query($sql_tagm_tag_themes_alter1);
	$wpdb->query($sql_tagm_tag_themes_alter2);

	$wpdb->query($sql_tagm_tag_theme_items_alter1);
	$wpdb->query($sql_tagm_tag_theme_items_alter2);
	$wpdb->query($sql_tagm_tag_theme_items_alter3);

	add_option( 'tags_db_version', $tags_db_version );
}
//---------------------------------------------------------
function tags_install_data() {
	tags_insert_setting_data();
}
//---------------------------------------------------------
function tags_insert_setting_data()
{
	global $wpdb;

	$tbl_tagm_tag_settings = $wpdb->prefix . 'tagm_tag_settings';

	$wpdb->insert(
		$tbl_tagm_tag_settings,
		array(
			'tag_setting_ID' => 1,
			'tag_setting_title' => 'فعال بودن جعبه افزونه تگ',
			'tag_setting_status' => true,
		)
	);
	$wpdb->insert(
		$tbl_tagm_tag_settings,
		array(
			'tag_setting_ID' => 2,
			'tag_setting_title' => 'فعال بودن باکس تگ های اضافه شده',
			'tag_setting_status' => true,
		)
	);
	$wpdb->insert(
		$tbl_tagm_tag_settings,
		array(
			'tag_setting_ID' => 3,
			'tag_setting_title' => 'فعال بودن ورودی های زبان انگلیسی',
			'tag_setting_status' => true,
		)
	);
}
//---------------------------------------------------------
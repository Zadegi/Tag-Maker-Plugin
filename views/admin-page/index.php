<link rel="script" href=<?php echo TAGS_ASSETS . "/js/jquery.min.js" ?>>
<link rel="stylesheet" href=<?php echo TAGS_ASSETS . "/css/bootstrap.rtl.css" ?>>
<link rel="stylesheet" href=<?php echo TAGS_ASSETS . "/css/font-awesome.css" ?>>
<link rel="stylesheet" href=<?php echo TAGS_ASSETS . "/css/style.css" ?>>
<div class="wrap my-form">
    <div class="alert alert-secondary" role="alert">
        <h1 class="title">تنظیمات تگ ساز</h1>
    </div>
    <div class="row my-row">
		<?php tags_load_view( 'admin-page.setting', compact('settings')); ?>
    </div>
    <div class="row my-row">
        <div class="col-sm-6">
	        <?php tags_load_view( 'admin-page.add-edit-tag', compact('items')); ?>
        </div>
        <div class="col-sm-6">
	        <?php tags_load_view( 'admin-page.tags-list', compact('themes')); ?>
        </div>
    </div>
</div>





<?php
$text_area  = "";
$theme_name = "";
if ( isset( $_GET['tid'] ) ) :
	$theme_name = $_GET['theme_name'];

	global $wpdb, $table_prefix;
	$query       = "select * from {$table_prefix}tagm_tag_theme_items where tag_theme_IDRef = {$_GET['tid']}";
	$theme_items = $wpdb->get_results( $query );

	if ( $theme_items && count( $theme_items ) > 0 ) :
		$counter = 0;
		foreach ( $theme_items as $theme_item ) :
			$text_area = $text_area . $theme_item->tag_theme_item_tit;
			$counter ++;
			if ( $counter < count( $theme_items ) ) :
				$text_area = $text_area . "\n";
			endif;
		endforeach;
	endif;

endif;
?>
<div class="alert alert-dark text-center" role="alert">
    <h2 class="my-h2">ویرایش قالب</h2>
</div>
<form action="" method="post" class="was-validated">
    <div class="form-group">
        <label for="themeName">نام قالب :</label>
        <input type="text" class="form-control"
               id="themeName"
               name="themeName"
               placeholder="نام قالب"
               value="<?php echo $theme_name; ?>"
               required>
    </div>
    <div class="form-group">
        <label for="tagList">لیست تگ ها :</label>
        <textarea class="form-control"
                  id="tagList"
                  name="tagList"
                  rows="10"
                  required><?php echo $text_area; ?></textarea>
    </div>
	<?php submit_button( 'ذخیره قالب', 'primary mb-2 btn-block', 'save_tag_add_edit' ); ?>
</form>
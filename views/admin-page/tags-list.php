<div class="alert alert-dark text-center" role="alert">
    <h2 class="my-h2">قالب ها</h2>
</div>
<table class="table table-bordered table-hover text-center">
    <thead class="thead-color">
    <tr>
        <th class="th-lg"><i class="fa fa-codepen fa-lg pr-2" aria-hidden="true"></i>نام</th>
        <th class="th-lg"><i class="fa fa-list-ol fa-lg pr-2" aria-hidden="true"></i>تعداد تگ ها</th>
        <th class="th-lg"><i class="fa fa-edit fa-lg pr-2" aria-hidden="true"></i>ویرایش</th>
        <th class="th-lg"><i class="fa fa-trash-o fa-lg pr-2" aria-hidden="true"></i>حذف</th>
    </tr>
    </thead>
    <tbody>
	<?php if ( $themes && count( $themes ) > 0 ) : ?>
		<?php $counter = 1; ?>
		<?php foreach ( $themes as $theme ) : ?>
			<?php if ( $counter ++ % 2 == 0 ) : ?>
                <tr class="trow-color">
			<?php else : ?>
                <tr>
			<?php endif; ?>
            <td><?php echo $theme->tag_theme_title; ?></td>
            <td><?php echo $theme->tag_theme_no; ?></td>
            <td><a target="_self" title="ویرایش قالب"
                   href="<?php global $wp;
			       echo home_url( $wp->request ) . '/wp-admin/options-general.php?page=tag_maker_menu&tid='
			            . $theme->tag_theme_ID . '&theme_name=' . $theme->tag_theme_title; ?>">
                    <i class="fa fa-edit"></i>
                </a>
            </td>
            <td><a href="<?php global $wp;
				echo home_url( $wp->request ) . '/wp-admin/options-general.php?page=tag_maker_menu&tid='
				     . $theme->tag_theme_ID . '&theme_name=' . $theme->tag_theme_title
				     . '&mode=del'; ?>">
                    <i class="fa fa-trash"></i></a></td>
            </tr>
		<?php endforeach; ?>
	<?php else : ?>
        <tr>
            <div class="alert-danger text-center" role="alert">
                <p>هیچ قالبی در لیست وجود ندارد</p>
            </div>
        </tr>
	<?php endif; ?>
    </tbody>
</table>
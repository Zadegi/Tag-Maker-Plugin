<div class="col-sm-12">
    <div class="alert alert-dark text-center" role="alert">
        <h2 class="my-h2">تنظیمات</h2>
    </div>
    <form action="" method="post" class="form-group">
	    <?php if ($settings && count($settings) > 0) : ?>
		    <?php $counter = 1; ?>
		    <?php foreach ($settings as $setting) : ?>
                <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" class="custom-control-input"
                           id="<?php echo "checkbox" . (string)$counter; ?>"
                           name="<?php echo "checkbox" . (string)$counter; ?>"
					    <?php checked(1, $setting->tag_setting_status); ?>
                    >
                    <label class="custom-control-label"
                           for="<?php echo "checkbox" . (string)$counter; ?>">
					    <?php echo $setting->tag_setting_title; ?>
                    </label>
                </div>
			    <?php $counter++; ?>
		    <?php endforeach; ?>
	    <?php else : ?>
            <div class="alert-danger text-center" role="alert">
                <p>تنظیماتی برای افزونه یافت نشد</p>
            </div>
	    <?php endif; ?>
		<?php submit_button( 'ذخیره تنظیمات', 'primary', 'save_settings' ); ?>
    </form>
</div>

<link rel="stylesheet" href=<?php echo TAGS_ASSETS . "/css/post-style.css" ?>>
<form action="" method="post" id="ajax_tag_form">
	<?php if ( $set_view_fa_text_box == 1 ) : ?>
        <div class="form-group">
            <label for="fa_name">نام فارسی :</label>
            <input type="text"
                   class="postbox"
                   id="fa_name"
                   name="fa_name"
                   placeholder="{name}"
                   value="">
        </div>
	<?php endif; ?>
	<?php if ( $set_view_fa_text_box == 1 && $set_view_en_text_box == 1 ) : ?>
        <div class="form-group">
            <label for="en_name">نام اینگلیسی :</label>
            <input type="text"
                   class="postbox"
                   id="en_name"
                   name="en_name"
                   placeholder="{enname}"
                   value="">
        </div>
	<?php endif; ?>
	<?php if ( $set_view_fa_text_box == 1 ) : ?>
        <div class="form-group">
            <label for="fa_music">نام آهنگ فارسی :</label>
            <input type="text"
                   class="postbox"
                   id="fa_music"
                   name="fa_music"
                   placeholder="{music}"
                   value="">
        </div>
	<?php endif; ?>
	<?php if ( $set_view_fa_text_box == 1 && $set_view_en_text_box == 1 ) : ?>
        <div class="form-group">
            <label for="en_music">نام آهنگ اینگلیسی :</label>
            <input type="text"
                   class="postbox"
                   id="en_music"
                   name="en_music"
                   placeholder="{enmusic}"
                   value="">
        </div>
	<?php endif; ?>
    <div class="form-group">
        <label for="tag_themes_list">لیست قالب تگ ها :</label>
        <select name="tag_themes_list"
                id="tag_themes_list"
                class="postbox">
			<?php foreach ( $tag_themes as $tag_theme ) : ?>
                <option value="<?php echo $tag_theme->tag_theme_ID; ?>"><?php echo $tag_theme->tag_theme_title; ?></option>
			<?php endforeach; ?>
        </select>
    </div>

    <div class="form-group">
        <textarea class="postbox"
                  id="tagList"
                  name="tagList"
                  rows="10"
                  required></textarea>
    </div>

    <div class="clear">
        <button name="tag_create_btn" id="tag_create_btn" type="submit" class="button button-large tag_create_btn">ساخت
            تگ
        </button>
    </div>
    <div>
        <p>لیست تگ های که بعد از ذخیره نوشته به قسمت برچسبها اضافه می شود:</p>
    </div>
    <div id="tag_list_created">
        <ul id="tag_ul_list" class="tagchecklist" role="list">

        </ul>
    </div>
    <div>
        <input type="hidden"
               id="tag_list_final_hidden"
               value="">
    </div>
</form>
<?php

//--------------------------------------------------------------
function tags_load_view($view, $params = array())
{
	$view = str_replace('.', DIRECTORY_SEPARATOR, $view);
	$viewFilePath = TAGS_VIEWS . $view . '.php';

	if(file_exists($viewFilePath) && is_readable($viewFilePath))
	{
		!empty($params) ? extract($params) : null;
		include $viewFilePath;
	}
}
//--------------------------------------------------------------
<?php
/*
Plugin Name: تگ ساز
Plugin URI: http://mzadegi.ir
Description: با استفاده از این پلاگین می توانید به راحتی برچسبهای خودتون رو برای نوشته ها ایجاد کنید
Version: 1.0.0
Author: مجید زادگی
Author URI: http://mzadegi.ir/
*/

// check security plugin
defined( 'ABSPATH' ) || die( 'access denied!' );

// declare constant in plugin
define( 'TAGS_DIR', plugin_dir_path( __FILE__ ) );
define( 'TAGS_URL', plugin_dir_url( __FILE__ ) );
define( 'TAGS_ASSETS', addslashes( TAGS_URL . "assets/" ) );
define( 'TAGS_VIEWS_URL', addslashes( TAGS_URL . "views/" ) );

define( 'TAGS_VIEWS', addslashes( TAGS_DIR . "views/" ) );
define( 'TAGS_INC', addslashes( TAGS_DIR . 'inc/' ) );
define( 'TAGS_ADMIN', addslashes( TAGS_DIR . 'control/admin/' ) );
define( 'TAGS_POST', addslashes( TAGS_DIR . 'control/post/' ) );

define('TAGS_DB_VERSION',1);

//----------------------------------------------------------------
//----------------------------------------------------------------
include TAGS_INC . 'functions.php';
include TAGS_ADMIN . 'admin.php';
include TAGS_POST . 'index.php';

//----------------------------------------------------------------
//----------------------------------------------------------------

//------------------------------------------------------------------------


//***////
function tag_maker_Deactivation() {

}

register_deactivation_hook( __FILE__, 'tag_maker_Deactivation' );
//------------------------------------------------------------------------

function tag_maker_activation()
{
	include TAGS_DIR . "upgrade.php";
}
register_activation_hook( __FILE__, 'tag_maker_activation' );
